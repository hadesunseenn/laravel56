<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabUser extends Model
{
    protected $fillable = [
       'name', 'phone_number','user_id', 'address', 'sex', 'age'
    ];

    public function reports()
    {
        return $this->hasMany('App\Report','customer_id');
    }
}
