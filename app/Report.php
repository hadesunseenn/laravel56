<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
       'user_id', 'test_id','doctor_id', 'customer_id'
    ];

    
    public function reportMeta()
    {
        return $this->hasMany('App\ReportsMeta');
    }

    public function doctor() {
    	return $this->belongsTo('App\Doctor');
    }

    public function patient() {
    	return $this->belongsTo('App\LabUser');
    }
}
