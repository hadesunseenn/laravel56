<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportsMeta extends Model
{
	protected $table = 'reports_meta';
    protected $fillable = [
       'user_id', 'report_id','field_name', 'field_value'
    ];
}
