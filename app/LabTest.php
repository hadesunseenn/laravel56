<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabTest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lab_test';
    // protected $primaryKey = 'id';

    protected $fillable = [
		'user_id', 'test_name'
    ];

}
