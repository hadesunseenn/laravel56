<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;
use App\Report;
use PDF;

class CreateReportPdfController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'role:Admin|labowner']);     
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $reportId = $request->id;
        Report::findOrFail($reportId);


        $user_id = Auth::user()->id;

        $reportData = \DB::table('reports')
            ->join('doctors', 'reports.doctor_id', '=', 'doctors.id')
            ->join('lab_users', 'reports.customer_id', '=', 'lab_users.id')
            ->join('lab_test', 'reports.test_id', '=', 'lab_test.id')
            ->select('reports.*', 'doctors.name as doctorName', 'lab_users.name as customerName', 'lab_users.sex', 'lab_test.test_name as testName')
            ->where('reports.user_id', $user_id)
            ->where('reports.id', $reportId)
            ->orderby('id', 'desc')
            ->first();

        $reportMeta = Report::find($reportId)->reportMeta;

            
            // $pdf = \PDF::loadView('createReportPdf.index',compact('reportData','reportMeta'));
            // return $pdf->download('pdf.pdf');
        


        // dd($reportMeta);
        return view('createReportPdf.index', compact('reportData', 'reportMeta'));



    }


    public function createPdf(Request $request)
    {

        $reportId = $request->id;
        $user_id = Auth::user()->id;

        $reportData = \DB::table('reports')
            ->join('doctors', 'reports.doctor_id', '=', 'doctors.id')
            ->join('lab_users', 'reports.customer_id', '=', 'lab_users.id')
            ->join('lab_test', 'reports.test_id', '=', 'lab_test.id')
            ->select('reports.*', 'doctors.name as doctorName', 'lab_users.name as customerName', 'lab_users.sex', 'lab_test.test_name as testName')
            ->where('reports.user_id', $user_id)
            ->where('reports.id', $reportId)
            ->orderby('id', 'desc')
            ->first();

        $reportMeta = Report::find($reportId)->reportMeta;

            
            $pdf = \PDF::loadView('createReportPdf.download',compact('reportData','reportMeta'));
            return $pdf->download('report-'.$reportId.'.pdf');
        


        // dd($reportMeta);
        return view('createReportPdf.download', compact('reportData', 'reportMeta'));



    }

    
}
