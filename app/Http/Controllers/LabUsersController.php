<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//additional  classes
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
// use Illuminate\Support\Facades\DB;

use App\LabUser;
use App\Report;
class LabUsersController extends Controller
{
    use HasRoles;

    public function __construct() {
        $this->middleware(['auth', 'role:Admin|labowner']);     
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $allLabusers = LabUser::orderby('name', 'asc')->paginate(10);
        
        return view('labUsers.index', compact('allLabusers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('labUsers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'name'=>'required|max:200'
        ]);

        $name = $request->input('name');
        $phone_number = $request->input('phone_number');
        $address = $request->input('address');
        $sex = $request->input('sex');
        $age = $request->input('age');

        $user_id = Auth::user()->id;
        
        // $post = LabTest::create($request->only('user_id'));
        $test = LabUser::create(['user_id' => $user_id, 'name' => $name, 'phone_number' => $phone_number, 'address' => $address, 'sex' => $sex, 'age' => $age]);
        

        //Display a successful message upon save
        return redirect()->route('lab-users.index')
            ->with('flash_message', 'Customer'. $name.' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = Auth::user()->id;
        $customer = LabUser::where('user_id', '=', $user_id)->findOrFail($id);
        
        // print_r($customer);
        // dd();
        $customerReports = Report::where('customer_id', '=', $id)->orderby('id', 'desc')->paginate(10);
        // $customerReports = LabUser::find($id)->reports;
        return view ('labUsers.show', compact('customer','customerReports'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validating title and body field
        $this->validate($request, [
            'name'=>'required|max:200',
        ]);

        $name = $request->input('name');
        $phone_number = $request->input('phone_number');
        $address = $request->input('address');
        $sex = $request->input('sex');
        $age = $request->input('age');

        $user_id = Auth::user()->id;

        \DB::table('lab_users')->where('id', $id)->where('user_id', $user_id)->update(['name' => $name, 'phone_number' => $phone_number, 'address' => $address, 'sex' => $sex, 'age' => $age]);

        return redirect()->route('lab-users.show', 
            $id)->with('flash_message', 
            'Customer , '. $name.' updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
