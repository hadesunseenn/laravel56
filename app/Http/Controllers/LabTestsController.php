<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

// use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
// use Spatie\Permission\Middlewares\RoleMiddleware;
// use App\Role;

use App\LabTest;
use App\LabTestFormValue;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LabTestsController extends Controller
{
	use HasRoles;

    public function __construct() {
    	$this->middleware(['auth', 'role:Admin|labowner']);
    	// $this->middleware(['auth', '']); 
        // $this->middleware(['auth', 'isAdmin']);
        // print_r(auth()->user());
        // $this->middleware(['role: Admin']); 
        
        // if (!Auth::user()->hasRole('labowner|Admin')) {
        //     abort('401');
        // }
        // die();
        // $user->hasAllRoles(Role::all());
     //    $user = Auth::user();
    	// print_r($user->hasRole('labowner'));
    	// die('in cons');
    }

    public function index()
    {
    	 // print_r(Auth::user()->id);
    	 $user_id = Auth::user()->id;

    	 // print_r($user->hasRole('labowner'));
    	 // print_r($user->hasAnyRole('labowner|Admin'));
    	 // print_r(Auth::user()->hasRole('labowner|Admin'));
    	 // print_r(auth()->user());
    	// echo "hi there";


        // $allForms = DB::table('lab_test')
        //     ->join('lab_test_form_values', 'lab_test_form_values.form_id', '=', 'lab_test.id')
        //     // ->join('orders', 'users.id', '=', 'orders.user_id')
        //     ->select('lab_test.*', 'lab_test_form_values.field_constituent', 'lab_test_form_values.field_unit','lab_test_form_values.field_normal_range','lab_test_form_values.field_name')
        //     ->where('lab_test.user_id', '=', $user_id)
        //     ->get();

        //     print_r($allForms);

        $tests = LabTest::orderby('id', 'desc')->paginate(10); //show only 5 items at a time in descending order
        // print_r($tests);
        // $tests = DB::table('lab_test')->paginate(1);
        
    	return view('labTest.index', compact('tests'));
    }

    public function create()
    {
        return view('labTest.create');
    }

    public function store(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'test_name'=>'required|max:200',
        ]);

        $fields = $request->input('field');



        $test_name = $request->input('test_name');
        $user_id = Auth::user()->id;
        
        // $post = LabTest::create($request->only('user_id'));
        $test = LabTest::create(['user_id' => $user_id, 'test_name' => $test_name]);
        $testId = $test->id;

        $fieldArr = [];
        if( count($fields) > 0 ) {
            foreach ($fields as $key => $value) {
                
                $now = Carbon::now()->toDateTimeString();
                $fieldArr[] = array(
                    'user_id'=>$user_id,
                    'form_id'=>$testId,
                    'field_name'=> $key,
                    'field_constituent'=> $value['constituent'],
                    'field_unit'=> $value['unit'],
                    'field_normal_range'=> $value['range'],
                    'created_at'=> $now,
                    'updated_at'=> $now
                );
            }
        }
        
        LabTestFormValue::insert($fieldArr);
        //now insert all rows in the lab_test_form_values table

        //Display a successful message upon save
        return redirect()->route('lab-tests.index')
            ->with('flash_message', 'Lab Test '. $test_name.' created');
    }

    public function show($id)
    {   
        $user_id = Auth::user()->id;
        $test = LabTest::findOrFail($id); //Find test of id = $id
        $test_name = $test->test_name;

        $formFields = DB::table('lab_test_form_values')
                ->select('field_name', 'field_constituent', 'field_unit', 'field_normal_range')
                ->where('user_id', '=', $user_id)
                ->where('form_id', '=', $id)
                ->get();

        // print_r($formFields);

        return view ('labTest.show', compact('test_name', 'formFields', 'test'));
    }

    public function update(Request $request, $id)
    {
        //Validating title and body field
        $this->validate($request, [
            'test_name'=>'required|max:200',
        ]);

        $fields = $request->input('field');



        $test_name = $request->input('test_name');
        $user_id = Auth::user()->id;

        DB::table('lab_test')->where('id', $id)->where('user_id', $user_id)->update(['test_name' => $test_name]);

        //Display a successful message upon save
        // return redirect()->route('lab-tests.index')
            // ->with('flash_message', 'Lab Test '. $test_name.' created');
        //update test fields
        // DB::enableQueryLog();  
        if( count($fields) > 0 ) {
            foreach ($fields as $key => $value) {
                $count = LabTestFormValue::where('field_name', $key)->where('form_id', $id)->count();
                // print_r($count);
                $now = Carbon::now()->toDateTimeString();
                if( $count > 0 ) {
                    //update field values
                    $fieldArr= array(
                        'field_constituent'=> $value['constituent'],
                        'field_unit'=> $value['unit'],
                        'field_normal_range'=> $value['range'],
                        'created_at'=> $now,
                        'updated_at'=> $now
                    );
                    DB::table('lab_test_form_values')->where('field_name', $key)->where('form_id', $id)->update($fieldArr);
                } else {
                    //this is new field, insert it
                    $fieldArr = array(
                        'user_id'=>$user_id,
                        'form_id'=>$id,
                        'field_name'=> $key,
                        'field_constituent'=> $value['constituent'],
                        'field_unit'=> $value['unit'],
                        'field_normal_range'=> $value['range'],
                        'created_at'=> $now,
                        'updated_at'=> $now
                    );
                    LabTestFormValue::insert($fieldArr);
                }
            }
        }
        // dd(DB::getQueryLog());


        return redirect()->route('lab-tests.show', 
            $id)->with('flash_message', 
            'Lab Test , '. $test_name.' updated successfully.');
    }
}
