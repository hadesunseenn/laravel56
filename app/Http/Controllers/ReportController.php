<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use App\Report;
use App\ReportsMeta;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'role:Admin|labowner']);     
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        // $allReports = Report::orderby('id', 'asc')->paginate(10);
        // $allReports = Report::orderby('id', 'asc')->paginate(10);
        

        $allReports = \DB::table('reports')
            ->join('doctors', 'reports.doctor_id', '=', 'doctors.id')
            ->join('lab_users', 'reports.customer_id', '=', 'lab_users.id')
            ->join('lab_test', 'reports.test_id', '=', 'lab_test.id')
            ->select('reports.*', 'doctors.name as doctorName', 'lab_users.name as customerName', 'lab_test.test_name as testName')
            ->where('reports.user_id', $user_id)
            ->orderby('id', 'desc')
            ->paginate(10);
            // ->get();

        // dd($allReports);
        return view('report.index', compact('allReports'));

        //this works
        //App\Doctor::find(1)->reports
        //App\Report::find(1)->doctor
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $test_id = $request->all();
        // print_r($_GET['test_id']);
        // $test_id = Input::get('test_id');
        // $test_id = $request->all();
        $test_id = $request->query('test_id');
        // dd($test_id);
        $formFields = [];
        $user_id = Auth::user()->id;
        $allTests = \DB::table('lab_test')
                ->select('test_name', 'id')
                ->where('user_id', '=', $user_id)
                ->get();


        $allDoctors = \DB::table('doctors')
                ->select('name', 'id')
                ->where('user_id', '=', $user_id)
                ->get();

        $allPatients = \DB::table('lab_users')
                ->select('name', 'id')
                ->where('user_id', '=', $user_id)
                ->get();

        if( !empty($test_id) ) {
            // echo 'in if';
            $formFields = \DB::table('lab_test_form_values')
                ->select('field_name', 'field_constituent','field_unit', 'field_normal_range')
                ->where('user_id', '=', $user_id)
                ->where('form_id', '=', $test_id)
                ->get();
        }

        // print_r($formFields);
        return view('report.create', compact('allTests', 'allDoctors', 'allPatients','formFields', 'test_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'test_id'=>'required|integer',
            'doctor_id'=>'required|integer',
            'patient_id'=>'required|integer',
        ]);

        $fields = $request->input('field');


        $test_id = $request->input('test_id');
        $doctor_id = $request->input('doctor_id');
        $patient_id = $request->input('patient_id');
        $user_id = Auth::user()->id;
        
        $reportData = [
            'user_id' => $user_id,
            'test_id' => $test_id,
            'doctor_id' => $doctor_id,
            'customer_id' => $patient_id
            ];
        // $post = LabTest::create($request->only('user_id'));
        $report = Report::create($reportData);
        $reportId = $report->id;

        $fieldArr = [];
        if( count($fields) > 0 ) {
            foreach ($fields as $key => $value) {
                
                $now = Carbon::now()->toDateTimeString();
                $fieldArr[] = array(
                    'user_id'=>$user_id,
                    'report_id'=>$reportId,
                    'field_name'=> $key,
                    'field_value'=> $value['value'],
                    'field_constituent'=> $value['field_constituent'],
                    'field_normal_range'=> $value['field_normal_range'],
                    'field_unit'=> $value['field_unit'],
                    'created_at'=> $now,
                    'updated_at'=> $now
                );
            }
        }
        
        ReportsMeta::insert($fieldArr);
        //now insert all rows in the lab_test_form_values table

        //Display a successful message upon save
        return redirect()->route('reports.index')
            ->with('flash_message', 'Report created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
