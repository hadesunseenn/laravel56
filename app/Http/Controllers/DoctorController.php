<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//additional  classes
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
// use Illuminate\Support\Facades\DB;

use App\Doctor;
class DoctorController extends Controller
{
    use HasRoles;

    public function __construct() {
        $this->middleware(['auth', 'role:Admin|labowner']);     
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->id;
        $allDoctors = Doctor::orderby('name', 'asc')->paginate();
        
        return view('doctor.index', compact('allDoctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('doctor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validating title and body field
        $this->validate($request, [
            'name'=>'required|max:200'
        ]);

        $phone_number = $request->input('phone_number');
        $name = $request->input('name');

        $user_id = Auth::user()->id;
        
        // $post = LabTest::create($request->only('user_id'));
        $test = Doctor::create(['user_id' => $user_id, 'name' => $name, 'phone_number' => $phone_number]);
        

        //Display a successful message upon save
        return redirect()->route('doctors.index')
            ->with('flash_message', 'Doctor'. $name.' created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = Auth::user()->id;
        $doctor = Doctor::where('user_id', '=', $user_id)->findOrFail($id);
        
        return view ('doctor.show', compact('doctor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validating title and body field
        $this->validate($request, [
            'name'=>'required|max:200',
        ]);

        $phone_number = $request->input('phone_number');
        $name = $request->input('name');

        $user_id = Auth::user()->id;

        \DB::table('doctors')->where('id', $id)->where('user_id', $user_id)->update(['name' => $name, 'phone_number' => $phone_number]);

        return redirect()->route('doctors.show', 
            $id)->with('flash_message', 
            'Doctor , '. $name.' updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
