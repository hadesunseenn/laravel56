<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LabTestFormValue extends Model
{
    protected $table = 'lab_test_form_values';

    protected $fillable = [
		'user_id','form_id','field_name','field_constituent','field_normal_range'
    ];
}
