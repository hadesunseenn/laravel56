<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = [
       'name', 'phone_number','user_id'
    ];

    public function reports()
    {
        return $this->hasMany('App\Report');
    }
}
