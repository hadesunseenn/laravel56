<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLabusersAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lab_users', function (Blueprint $table) {
            $table->string('sex', 1);
            $table->string('age', 10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lab_users', function (Blueprint $table) {
            $table->dropColumn('sex');
            $table->dropColumn('age');
        });
    }
}
