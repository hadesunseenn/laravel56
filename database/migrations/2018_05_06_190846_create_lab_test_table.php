<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lab_test', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            
        });

        Schema::table('lab_test', function($table) {
           $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
       });

        Schema::create('lab_test_form_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('form_id')->unsigned();
            $table->string('field_name', 100);
            $table->string('field_constituent', 100);
            $table->string('field_unit', 100);
            $table->string('field_normal_range', 100);
            $table->timestamps();
        });

        Schema::table('lab_test_form_values', function($table) {
           $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('form_id')->references('id')->on('lab_test')->onDelete('cascade');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lab_test');
        Schema::dropIfExists('lab_test_form_values');
    }
}
