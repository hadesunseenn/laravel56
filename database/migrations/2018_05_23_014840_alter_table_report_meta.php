<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReportMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_meta', function (Blueprint $table) {
            $table->string('field_constituent', 100);
            $table->string('field_normal_range', 100);
            $table->string('field_unit', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_meta', function (Blueprint $table) {
            $table->dropColumn('field_constituent');
            $table->dropColumn('field_normal_range');
            $table->dropColumn('field_unit');
        });
    }
}
