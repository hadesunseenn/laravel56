@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Add New Customer
                    </div>
                    <div class="card-body">
                        {{ Form::open(array('route' => 'lab-users.store')) }}
                            <div class="row">
                                <div class="col">
                                    <label for="">Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Name">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Phone Number</label>
                                    <input type="text" class="form-control" name="phone_number" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Address</label>
                                    <textarea name="address" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Sex</label>
                                    <select name="sex" class="form-control">
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3">Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Age</label>
                                    <input type="text" class="form-control" name="age" placeholder="Age">
                                </div>
                            </div>
                            
                            <div class="row mt-3">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6 text-right">
                                    {{ Form::submit('Add', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                                
                            </div>
                            {{ Form::close() }}
                    </div>
                </div>

                
                
            </div>
        </div>
@endsection
