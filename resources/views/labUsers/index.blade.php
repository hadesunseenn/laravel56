@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="page-header">
                    <h1>All Customers</h1>      
                </div>
                <p>Page {{ $allLabusers->currentPage() }} of {{ $allLabusers->lastPage() }}</p>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Address</th>
                            <th>Sex</th>
                            <th>Age</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($allLabusers as $post)
                        <tr>
                            <td><a href="{{ route('lab-users.show', $post->id ) }}">{{ $post->name }}</a></td>
                            <td>{{ $post->phone_number }}</td>
                            <td>{{ $post->address }}</td>
                            <td>
                                @php
                                    if( $post->sex == 1 ) {
                                        $sex = 'Male';
                                    } elseif ( $post->sex == 2 ) {
                                        $sex = 'Female';
                                    } else {
                                        $sex = 'Other';
                                    }
                                @endphp
                                {{ $sex }}
                            </td>
                            <td>{{ $post->age }}</td>
                            
                        </tr>    
                        @endforeach
                    </tbody>
                </table>
                
                <p>{{ $allLabusers->links() }}</p>
            </div>
        </div>
@endsection