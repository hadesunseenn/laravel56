@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">
    <style type="text/css">
    .bold-label{
        font-size: 16px;
        font-weight: bold;
    }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Add New Report
                    </div>
                    <div class="card-body">
                        {{ Form::open(array('route' => 'reports.store')) }}
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="" class="bold-label">Test</label>
                                    <select name="test_id" class="form-control selectpicker" data-live-search="true" id="testSelect">
                                        <option value="">Select Test Name</option>
                                    @foreach($allTests as $test)
                                    	@if( $test_id == $test->id )
                                    		@php($selected = 'selected')
                                    	@else
                                    		@php($selected = '')
                                    	@endif
                                        <option data-tokens="{{ $test->test_name }}" value="{{ $test->id }}" {{ $selected }}>{{ $test->test_name }}</option>
                                    @endforeach
                                    </select>
                                </div>

                                <div class="col-md-6">
                                    <label for="" class="bold-label">Doctor</label>
                                    <select name="doctor_id" class="form-control selectpicker" data-live-search="true">
                                    @foreach($allDoctors as $doctor)
                                        <option data-tokens="{{ $doctor->name }}" value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            

                            <div class="row mt-3">
                                
                            </div>
                            

                            <div class="row mt-">
                                <div class="col">
                                    <label for="" class="bold-label">Patient</label>
                                    <select name="patient_id" class="form-control selectpicker" data-live-search="true">
                                    @foreach($allPatients as $patient)
                                        <option data-tokens="{{ $patient->name }}" value="{{ $patient->id }}">{{ $patient->name }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>

                            @if( count( $formFields ) > 0 )
                            <div class="card mt-5">
                                <div class="card-body">
  
                            <div class="row">
                                <div class="col">
                                    <h3 class="text-danger">Test Details</h3>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-2 text-center">
                                    <label class="bold-label text-primary">Constituents</label>
                                </div>
                                <div class="col-md-4 text-center">
                                    <label class="bold-label text-primary">Value</label>
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label text-primary">Unit</label>
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label text-primary">Normal Range</label>
                                </div>
                            </div>
                            @foreach($formFields as $row)
                            <div class="row mt-3">
                                <div class="col-md-2 text-center">
                                    <label class="bold-label"> {{ $row->field_constituent }}</label>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="field[{{ $row->field_name }}][value]" class="form-control">
                                    <input type="hidden" name="field[{{ $row->field_name }}][field_constituent]" value="{{ $row->field_constituent }}">
                                    <input type="hidden" name="field[{{ $row->field_name }}][field_unit]" value="{{ $row->field_unit }}">
                                    <input type="hidden" name="field[{{ $row->field_name }}][field_normal_range]" value="{{ $row->field_normal_range }}">
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label">{{ $row->field_unit }}</label>
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label">{{ $row->field_normal_range }}</label>
                                </div>
                            </div>
                            @endforeach
                            </div>
                            </div>
                            @endif

                            @if (app('request')->has('test_id'))
                            <div class="row mt-3">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6 text-right">
                                    {{ Form::submit('Add', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                                
                            </div>
                            @endif
                            {{ Form::close() }}

                    </div>
                </div>

                
                
            </div>
        </div>
@endsection

@section('additional_js_scripts')
<script type="text/javascript">
$(document).ready(function(){
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.js')
              .done(enableSelectpicker);
});

function enableSelectpicker(){
    // $('.selectpicker').selectpicker();
    $('#testSelect').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
        // console.log(e.target.value);
        var selectedTest = e.target.value;
        window.location.href = "/reports/create?test_id="+selectedTest;
    });
}
</script>
@endsection