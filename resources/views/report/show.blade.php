@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Customer Details
                    </div>
                    <div class="card-body">
                        {{ Form::open(array('route' => array('lab-users.update', $customer->id), 'method' => 'PUT')) }}
                            
                            <div class="row">
                                <div class="col">
                                    <label for="">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $customer->name }}">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Phone Number</label>
                                    <input type="text" class="form-control" name="phone_number" value="{{ $customer->phone_number }}">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Address</label>
                                    <textarea name="address" class="form-control" rows="5">{{ $customer->address }}</textarea>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Sex</label>
                                    <select name="sex" class="form-control">
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                        <option value="3">Other</option>
                                        @php
                                            $sexArr = ['','Male', 'Female', 'Other'];
                                            $sex = $customer->sex;
                                            for( $i=1; $i <= 3; $i++ ) {
                                                if( $i == $sex ) {
                                                    $selected = 'selected';
                                                } else {
                                                    $selected = '';
                                                }
                                                echo '<option value="'.$i.'" '.$selected.'>'.$sexArr[$i].'</option>';
                                            }
                                        @endphp
                                    </select>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Age</label>
                                    <input type="text" class="form-control" name="age" value="{{ $customer->age }}">
                                </div>
                            </div>
                            
                            <div class="row mt-3">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6 text-right">
                                    {{ Form::submit('Update', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                                
                            </div>
                            {{ Form::close() }}

                        
                    </div>
                </div>

                
                
            </div>
        </div>
@endsection