@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="page-header">
                    <h1>All Reports</h1>      
                </div>
                <div class="row justify-content-center mb-3">
                    <div class="col-md-8">
                        Page {{ $allReports->currentPage() }} of {{ $allReports->lastPage() }}
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="search" placeholder="Search Report">
                    </div>
                </div>
                
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Report ID</th>
                            <th scope="col">Test Name</th>
                            <th scope="col">Patient Name</th>
                            <th scope="col">Doctor Name</th>
                            <th scope="col">Created Date</th>
                            <th scope="col">View Report</th>
                            <th scope="col">Download Report</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($allReports as $post)
                        <tr>
                            {{-- <td><a href="{{ route('reports.show', $post->id ) }}">{{ $post->id }}</a></td> --}}
                            <td>{{ $post->id }}</td>
                            <td>{{ $post->testName }}</td>
                            <td>{{ $post->customerName }}</td>
                            <td>{{ $post->doctorName }}</td>
                            <td>{{ \Carbon\Carbon::parse($post->created_at)->format('d-m-Y')}}</td>
                            <td><a target="_blank" href="{{url('/show-report/'.$post->id)}}">View</a></td>
                            <td><a target="_blank" href="{{url('/create-pdf/'.$post->id)}}">Download</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <p>
                    
                    {{ $allReports->links() }}
                </p>
            </div>
        </div>
@endsection


@section('additional_js_scripts')

<script>
$(document).ready(function(){
    
    $('#search').keyup(function(e){
        if(e.keyCode == 13) {
            var reportId = $(this).val();
            
            // window.location.href='/show-report/'+reportId;
            if( reportId )
                window.open(
              '/show-report/'+reportId,
              '_blank' // <- This is what makes it open in a new window.
            );
        }
    });

    
});


// console.log(makeid());
</script>
@endsection