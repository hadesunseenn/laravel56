@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ $test_name }} Test Details
                    </div>
                    <div class="card-body">
                        <!-- <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a> -->
                        {{ Form::open(array('route' => array('lab-tests.update', $test->id), 'method' => 'PUT')) }}
                            <div class="row">
                                <div class="col">
                                    <label for="">Test Name</label>
                                    <input type="text" class="form-control" name="test_name" value="{{ $test_name }}">
                                </div>
                            </div>
                            @if(count($formFields) > 0)
                                @foreach ($formFields as $key => $value)
                                    @php
                                        if( $loop->first ) {
                                            $class='mt-5';
                                        } else {
                                            $class='mt-2';
                                        }
                                    @endphp
                                    <div class="row {{$class}}">
                                        <div class="col text-center">
                                            @if( $loop->first )
                                            <label for="" class=""><strong>Constituent</strong></label>
                                            @endif
                                            <input type="text" name="field[{{ $value->field_name }}][constituent]" class="form-control" value="{{$value->field_constituent}}">
                                        </div>
                                        <div class="col text-center">
                                            @if( $loop->first )
                                            <label for="" class=""><strong>Unit</strong></label>
                                            @endif
                                            <input type="text" name="field[{{ $value->field_name }}][unit]" class="form-control" value="{{$value->field_unit}}">
                                        </div>
                                        <div class="col text-center">
                                            @if( $loop->first )
                                            <label for="" class=""><strong>Range</strong></label>
                                            @endif
                                            <input type="text" name="field[{{ $value->field_name }}][range]" class="form-control" value="{{$value->field_normal_range}}">
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <label for="" class=""><strong>Constituent</strong></label>
                                        <input type="text" name="" class="form-control constituent" placeholder="Constituent name">
                                    </div>
                                    <div class="col text-center">
                                        <label for="" class=""><strong>Unit</strong></label>
                                        <input type="text" name="" class="form-control unit" placeholder="Unit name">
                                    </div>
                                    <div class="col text-center">
                                        <label for="" class=""><strong>Range</strong></label>
                                        <input type="text" name="" class="form-control range" placeholder="Range name">
                                    </div>
                                </div>
                            @endif

                            <div class="append-new-row"></div>

                            
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <a href="javascript:void(0)" class="btn btn-primary add_row">Add Row</a>
                                </div>
                                <div class="col-md-6 text-right">
                                    {{ Form::submit('Update', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                                
                            </div>
                            {{ Form::close() }}

                        <div class="row clone-row invisible mt-2">
                            <div class="col text-center">
                                <input type="text" name="" class="form-control constituent" placeholder="Constituent name">
                            </div>
                            <div class="col text-center">
                                <input type="text" name="" class="form-control unit" placeholder="Unit name">
                            </div>
                            <div class="col text-center">
                                <input type="text" name="" class="form-control range" placeholder="Range name">
                            </div>
                        </div>
                    </div>
                </div>

                
                
            </div>
        </div>
@endsection


@section('additional_js_scripts')

<script>
$(document).ready(function(){
    
    random_id = makeid();
    field_id = 'field[field_'+random_id+']';

    $('form').find('.constituent').attr('name', field_id+'[constituent]');
    $('form').find('.unit').attr('name', field_id+'[unit]');
    $('form').find('.range').attr('name', field_id+'[range]');

    $('.add_row').click(function(){
        // clone
        var new_row = $('.clone-row').clone();
        
        // random_id = makeid();
        new_row.removeClass('invisible clone-row');
        
        random_id = makeid();
        field_id = 'field[field_'+random_id+']';

        new_row.find('.constituent').attr('name', field_id+'[constituent]');
        new_row.find('.unit').attr('name', field_id+'[unit]');
        new_row.find('.range').attr('name', field_id+'[range]');

        console.log(new_row);
        $('.append-new-row').append(new_row);

    });

    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js')
              .done(validateForm);
    
});

function validateForm() {
    //validate form
    $( "form" ).validate({
        rules: {
            test_name: {
                required: true
            }
        }
    });
}
function makeid() {
    var text = "";
    var char = "abcdefghijklmnopqrstuvwxyz";
    var int = "0123456789";

    for (var i = 0; i < 5; i++)
        text += char.charAt(Math.floor(Math.random() * char.length));

    for (var i = 0; i < 5; i++)
        text += int.charAt(Math.floor(Math.random() * int.length));

  return text;
}

// console.log(makeid());
</script>
@endsection