@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="page-header">
                    <h1>All Tests</h1>      
                </div>
                <p>Page {{ $tests->currentPage() }} of {{ $tests->lastPage() }}</p>
                <ul class="list-group">
                    @foreach ($tests as $post)
                        <li class="list-group-item"><a href="{{ route('lab-tests.show', $post->id ) }}">{{ $post->test_name }}</a></li>
                    @endforeach
                </ul>
                <p>
                    <!-- {!! $tests->links() !!} -->
                    {{ $tests->links() }}
                </p>
            </div>
        </div>
@endsection