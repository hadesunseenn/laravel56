@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="page-header">
                    <h1>All Doctors</h1>      
                </div>
                <p>Page {{ $allDoctors->currentPage() }} of {{ $allDoctors->lastPage() }}</p>
                <ul class="list-group">
                    @foreach ($allDoctors as $post)
                        <li class="list-group-item"><a href="{{ route('doctors.show', $post->id ) }}">{{ $post->name }}</a></li>
                    @endforeach
                </ul>
                <p>
                    
                    {{ $allDoctors->links() }}
                </p>
            </div>
        </div>
@endsection