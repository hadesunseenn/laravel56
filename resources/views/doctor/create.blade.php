@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Add New Doctor
                    </div>
                    <div class="card-body">
                        {{ Form::open(array('route' => 'doctors.store')) }}
                            <div class="row">
                                <div class="col">
                                    <label for="">Doctor Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Name">
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <label for="">Doctor Phone</label>
                                    <input type="text" class="form-control" name="phone_number" placeholder="Phone Number">
                                </div>
                            </div>
                            
                            <div class="row mt-3">
                                <div class="col-md-6">
                                </div>
                                <div class="col-md-6 text-right">
                                    {{ Form::submit('Add', array('class' => 'btn btn-primary pull-right')) }}
                                </div>
                                
                            </div>
                            {{ Form::close() }}
                    </div>
                </div>

                
                
            </div>
        </div>
@endsection
