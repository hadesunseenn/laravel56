@extends('layouts.app')

@push('styles')
    <style type="text/css">
    .bold-label{
        font-size: 16px;
        font-weight: bold;
    }
    </style>
@endpush

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Report Data <a class="float-right" href="{{ url('/create-pdf/'.$reportData->id) }}">Download Report</a>
                    </div>
                    <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="" class="bold-label">Test Name: {{ $reportData->testName }}</label>
                                    
                                </div>

                                <div class="col-md-6">
                                    <label for="" class="bold-label">Doctor Name: {{ $reportData->doctorName }}</label>
                                    
                                </div>
                            </div>
                            

                            <div class="row mt-3">
                                
                            </div>
                            

                            <div class="row mt-">
                                <div class="col">
                                    <label for="" class="bold-label">Patient Name: {{$reportData->customerName}}</label>
                                </div>
                                <div class="col">
                                    @php
                                        $sex = $reportData->sex;
                                        if( $sex == 1 ) {
                                            $sex = 'Male';
                                        } elseif ( $sex == 2 ) {
                                            $sex = 'Female';
                                        } else {
                                            $sex = 'Other';
                                        }
                                    @endphp
                                    <label for="" class="bold-label">Sex: {{$sex}}</label>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <label for="" class="bold-label">Date: {{ \Carbon\Carbon::now()->format('d-m-Y')}}</label>
                                </div>

                                <div class="col-md-6">
                                    <label for="" class="bold-label">Time: {{ \Carbon\Carbon::now()->format('H:i:s l')}}</label>
                                </div>
                            </div>

                            @if( count( $reportMeta ) > 0 )
                            <div class="card mt-5">
                                <div class="card-body">
  
                            <div class="row">
                                <div class="col">
                                    <h3 class="text-danger">Test Details</h3>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-4 text-center">
                                    <label class="bold-label">Constituent</label>
                                </div>
                                <div class="col-md-2">
                                    <label class="bold-label">Value</label>
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label">Unit</label>
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label">Normal Range</label>
                                </div>
                            </div>
                            
                            @foreach($reportMeta as $row)
                            <div class="row mt-3">
                                <div class="col-md-4 text-center">
                                    <label class="bold-label"> {{ $row->field_constituent }}</label>
                                </div>
                                <div class="col-md-2">
                                    <label class="bold-label"> {{ $row->field_value }}</label>
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label">{{ $row->field_unit }}</label>
                                </div>
                                <div class="col-md-3  text-center">
                                    <label class="bold-label">{{ $row->field_normal_range }}</label>
                                </div>
                            </div>
                            @endforeach
                            </div>
                            </div>
                            @endif

                    </div>
                </div>

                
                
            </div>
        </div>
@endsection

@section('additional_js_scripts')

@endsection