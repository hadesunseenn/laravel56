
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Report</title>
    
    <style type="text/css">
        .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  /*width: 21cm;  
  height: 29.7cm; */
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 90px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #5D6975;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  /*background: url(dimension.png);*/
}

#project {
  /*float: left;*/
}

#company span, #project span {
  color: #5D6975;
  text-align: right;
  width: 102px;
  margin-right: 10px;
  /*display: inline-block;*/
  font-size: 1em;
}

#company {
  float: left;
  /*text-align: right;*/
}

#project div,
#company div {
  white-space: nowrap;
  font-size: 1em;
  vertical-align: top;  
}

table {
  width: 700px;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
}

table th {
  padding: 5px 20px;
  color: #5D6975;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-weight: normal;
}

table .service,
table .desc {
  text-align: center;
}

table td {
  padding: 10px;
  text-align: right;
}

table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
  text-align: center;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
    </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo" style="height:80px;">
        {{-- <img src="logo.png"> --}}
      </div>
      <h1>Report Data</h1>
      <div id="company">
      @php
            $sex = $reportData->sex;
            if( $sex == 1 ) {
                $sex = 'Male';
            } elseif ( $sex == 2 ) {
                $sex = 'Female';
            } else {
                $sex = 'Other';
            }
        @endphp
        <div><span>Test Name</span> {{ $reportData->testName }}</div>
        <div><span>Doctor Name</span> {{ $reportData->doctorName }}</div>
        <div><span>Patient Name</span> {{$reportData->customerName}}</div>
        <div><span>Sex</span> {{$sex}}</div>
      </div>
      <div id="project" style="float:right">
        <div><span>Reference Number</span> {{ $reportData->id }}</div>
        <div><span>Date</span> {{ \Carbon\Carbon::now()->format('d-m-Y')}}</div>
        <div><span>Time</span> {{ \Carbon\Carbon::now()->format('H:i:s l')}}</div>
      </div>
    </header>
    @if( count( $reportMeta ) > 0 )
    <main style="text-align: center;width:100%;">
      <table style="margin:0 auto;">
        <thead>
          <tr>
            <th class="service" align="center">Constituent</th>
            <th class="desc" align="center">Value</th>
            <th align="center">Unit</th>
            <th align="center">Normal Range</th>
          </tr>
        </thead>
        <tbody>
        @foreach($reportMeta as $row)
          <tr>
            <td class="service">{{ $row->field_constituent }}</td>
            <td class="desc">{{ $row->field_value }}</td>
            <td class="unit">{{ $row->field_unit }}</td>
            <td class="qty">{{ $row->field_normal_range }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {{-- <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div> --}}
    </main>
    @endif
    <footer>
      Report was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>






