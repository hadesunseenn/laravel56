<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
//Route::get('/home', 'HomeController@index');
//
// Route::get('/', 'PostController@index')->name('home');
Route::get('/', 'ReportController@index')->name('home');
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');
Route::resource('permissions', 'PermissionController');
Route::resource('posts', 'PostController');
Route::resource('lab-tests', 'LabTestsController');
Route::resource('doctors', 'DoctorController');
Route::resource('lab-users', 'LabUsersController');
Route::resource('reports', 'ReportController');
Route::get('show-report/{id}', 'CreateReportPdfController@index');
Route::get('create-pdf/{id}', 'CreateReportPdfController@createPdf');
// Route::get('lab-tests', 'LabTestsController@index')->name('lab-tests');


// Route::get('/lab-tests', [
//    'middleware' => 'role:Admin',
//    'uses' => 'LabTestsController@index',
// ]);
